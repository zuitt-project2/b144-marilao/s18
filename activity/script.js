
// 3
let trainer = {
	// 4
	name: 'Misty',
	age: 30,
	pokemon: ['Onyx','Gigglypuff','Snorlax','Psyduck'],
	friends:{
		Team1: ['Ash','Brock'],
		Team2: ['Jessie','James'],
		// 5
		talk: function(){
			console.log(this.pokemon[0] +','+ ' I choose you')
		}
	}

}
console.log(trainer)

// 6 dot notation
console.log('Result of dot notation:')
console.log(trainer.name)
// square bracket
console.log('Result of square bracket notation')
console.log(trainer['pokemon'])
// 7
console.log('Result of talk method')
trainer.talk = function() {
console.log(this.pokemon[0] +','+ ' I choose you')
}
trainer.talk()




// 8
function Pokemon(name, level){
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack =  level;


	//Methods

	this.tackle = function(target) {
		let myHealth = target.health - this.attack
		console.log(this.name + ' tackled' +' '+ target.name)
		console.log(target.name + " " + " health is now reduced to "+ " "+ myHealth)
		
		target.health = myHealth
	
	if(myHealth <= 0)
		{
		console.log(target.name + " " + 'fainted.')
		}
	}
}
let Onyx = new Pokemon('Onyx', 20)
let Gigglypuff = new Pokemon('Gigglypuff', 10)
let Snorlax = new Pokemon('Snorlax', 14)
let Psyduck = new Pokemon('Psyduck',3)

console.log(Onyx)
console.log(Gigglypuff)
console.log(Snorlax)


Onyx.tackle(Gigglypuff)
console.log(Onyx)
Snorlax.tackle(Psyduck)




